.PHONY: clobber syncdb server celery imprimodump

clobber:
	sqlite3 -line mysite.sqlite3.db 'DROP TABLE imprimo_jobsession;'
	python manage.py syncdb
	rm -f media/imprimo/*

syncdb:
	python manage.py syncdb

server:
	python manage.py runserver

celery:
	python manage.py celery worker --loglevel=info

imprimodump:
	sqlite3 -line mysite.sqlite3.db '.dump imprimo_jobsession'
